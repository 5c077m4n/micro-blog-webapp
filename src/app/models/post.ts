export interface Post {
	_id?: string;
	content?: string;
	userID?: string;
	usersVoteUp?: string[];
	usersVoteDown?: string[];
}
