import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';


@Injectable({ providedIn: 'root' }) export class LogService {
	private readonly URL = 'http://localhost:3000/logs';
	private readonly httpOptions: {};
	constructor(private http: HttpClient) {
		this.httpOptions = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json',
				'Access-Control-Allow-Origin': '*',
			})
		};
	}

	getAll(): Observable<any> {
		return this.http.get<string>(this.URL, this.httpOptions)
			.pipe(
				tap(logs => {
					if(!logs) console.error(
						`[LogService] There was an error in getting the logs.`
					);
				}),
				catchError(this.handleError('getAll'))
			);
	}
	add(log: string): Observable<any> {
		return this.http.post<any>(this.URL, { log }, this.httpOptions)
			.pipe(
				tap(resp => {
					if(!resp)
						console.error(`[LogService] There was an error in posting the logs.`);
				}),
				catchError(this.handleError('add'))
			);
	}
	clearAll(log: string): Observable<any> {
		return this.http.delete<any>(this.URL, {
			observe: 'response',
			...this.httpOptions
		})
			.pipe(
				tap(resp => (resp.status !== 204)?
					console.error(
						`[LogService] There was an error in deleting the logs.`
					) : undefined
				),
				catchError(this.handleError('clearLog'))
			);
	}

	/** @function handleError - Error handler */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			console.error(`[LogService.${operation}()] Error: ${error.message}`);
			return of(result as T);
		};
	}
}
