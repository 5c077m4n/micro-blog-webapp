import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

import { User } from '../models/user';
import { LocalStorageService } from './local-storage.service';


@Injectable({ providedIn: 'root' }) export class AuthService {
	private readonly URL = 'http://localhost:3000';
	private jwt: JwtHelperService;
	constructor(
		private http: HttpClient,
		private storage: LocalStorageService,
	) {
		this.jwt = new JwtHelperService();
	}

	public login(username: string, password: string): Observable<any> {
		return this.http.post<any>(
			`${this.URL}/login`, { username, password }
		)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[AuthService] There was an error in logging you in.`
					);
				}),
				catchError(this.handleError('login'))
			);
	}
	public register(user: User): Observable<any> {
		return this.http.post<any>(`${this.URL}/register`, user)
			.pipe(
				tap(resp => {
					if(!resp.data) console.error(
						`[AuthService] There was an error in registering you.`
					);
				}),
				catchError(this.handleError('register'))
			);
	}

	public getToken(): any {
		return this.storage.get('raw-token');
	}
	public isTokenExpired(): boolean {
		return this.jwt.isTokenExpired(this.getToken());
	}
	public getCurrentUser(): User {
		const userInfo = this.storage.get('current-user');
		return (userInfo)? userInfo.user : undefined;
	}
	public isLoggedIn(): boolean {
		return (this.getCurrentUser() && !this.isTokenExpired());
	}
	public logout(): void {
		this.storage.remove('raw-token');
		this.storage.remove('current-user');
	}

	/** @function handleError - Error handler */
	private handleError<T>(operation = 'operation', result?: T): (any) => Observable<T> {
		return (error: any): Observable<T> => {
			console.error(`[AuthService.${operation}()] Error: ${error.message}`);
			return of(result as T);
		};
	}
}
