import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { AuthService } from './auth.service';
import { Post } from '../models/post';


@Injectable({ providedIn: 'root' }) export class BlogService {
	private readonly URL = 'http://localhost:3000';
	constructor(
		private http: HttpClient,
		private auth: AuthService
	) {}

	public getPosts(): Observable<any> {
		return this.http.get<any>(`${this.URL}/posts`)
			.pipe(
				tap(resp => {
					if(!resp) return console.error(
						`[BlogService] There was an error in getting the post list.`
					);
				}),
				catchError(this.handleError('getPosts'))
			);
	}
	public getUser(): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.get<any>(`${this.URL}/users/${userID}`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[BlogService] There was an error in getting the user's info.`
					);
				}),
				catchError(this.handleError('getUser'))
			);
	}
	public getUserPosts(): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.get<any>(`${this.URL}/users/${userID}/posts`)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[BlogService] There was an error in getting your posts.`
					);
				}),
				catchError(this.handleError('getUserPosts'))
			);
	}
	public getUserTopPosts(): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.get<any>(
			`${this.URL}/users/${userID}/posts/top`
		)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[BlogService] There was an error in getting the top posts.`
					);
				}),
				catchError(this.handleError('getUserTopPosts'))
			);
	}
	public getUserPost(postID: string): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.get<any>(
			`${this.URL}/users/${userID}/posts/${postID}`
		)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[BlogService] There was an error in getting the post.`
					);
				}),
				catchError(this.handleError('getUserPost'))
			);
	}
	public addUserPost(post: Post): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.post<any>(
			`${this.URL}/users/${userID}/posts`,
			post
		)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[BlogService] There was an error in posting your post.`
					);
				}),
				catchError(this.handleError('addUserPost'))
			);
	}
	public editUserPost(post: Post): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.put<any>(
			`${this.URL}/users/${userID}/posts/${post._id}`,
			{ content: post.content }
		)
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[BlogService] There was an error in posting your post.`
					);
				}),
				catchError(this.handleError('addUserPost'))
			);
	}
	public voteUp(postID: string): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.put<any>(`${this.URL}/users/${userID}/posts/${postID}/vote-up`, {})
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[BlogService] There was an error in voting up.`
					);
				}),
				catchError(this.handleError('voteUp'))
			);
	}
	public removeVoteUp(postID: string): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.delete<any>(
			`${this.URL}/users/${userID}/posts/${postID}/vote-up`,
			{ observe: 'response' }
		)
			.pipe(
				tap(resp => {
					if(resp.status !== 204) return console.error(
						`[BlogService] There was an error in deleting your vote up.`
					);
				}),
				catchError(this.handleError('removeVoteUp'))
			);
	}
	public voteDown(postID: string): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.put<any>(`${this.URL}/users/${userID}/posts/${postID}/vote-down`, {})
			.pipe(
				tap(resp => {
					if(!resp.data) return console.error(
						`[BlogService] There was an error in voting down.`
					);
				}),
				catchError(this.handleError('voteDown'))
			);
	}
	public removeVoteDown(postID: string): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.delete<any>(
			`${this.URL}/users/${userID}/posts/${postID}/vote-down`,
			{ observe: 'response' }
		)
			.pipe(
				tap(resp => {
					if(resp.status !== 204) return console.error(
						`[BlogService] There was an error in deleting your vote down.`
					);
				}),
				catchError(this.handleError('removeVoteDown'))
			);
	}
	public deleteUserPost(postID: string): Observable<any> {
		const userID = this.auth.getCurrentUser()._id;
		return this.http.delete<any>(
			`${this.URL}/users/${userID}/posts/${postID}`,
			{ observe: 'response' }
		)
			.pipe(
				tap(resp => {
					if(resp.status !== 204) return console.error(
						`[BlogService] There was an error in deleting your post.`
					);
				}),
				catchError(this.handleError('deleteUserPost'))
			);
	}

	/** @function handleError - Error handler */
	private handleError<T>(operation = 'operation', result?: T): (any) => Observable<T> {
		return (error: any): Observable<T> => {
			console.error(`[BlogService.${operation}()] Error: ${error.message}`);
			return of(result as T);
		};
	}
}
