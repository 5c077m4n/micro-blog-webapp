import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatMenuModule } from '@angular/material/menu';
import { MatBadgeModule } from '@angular/material/badge';
import { MatDividerModule } from '@angular/material/divider';


const modules = [
	MatButtonModule,
	MatCardModule,
	MatExpansionModule,
	MatProgressSpinnerModule,
	MatFormFieldModule,
	MatInputModule,
	MatIconModule,
	MatSnackBarModule,
	MatListModule,
	MatToolbarModule,
	MatBottomSheetModule,
	MatMenuModule,
	MatBadgeModule,
	MatDividerModule,
];

@NgModule({
	imports: [
		CommonModule,
		...modules
	],
	exports: [
		...modules
	],
	declarations: []
})
export class StyleModule {}
