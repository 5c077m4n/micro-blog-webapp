import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { httpInterceptorProviders } from './interceptors';
import { AppRoutingModule } from './app-routing.module';
import { StyleModule } from './material/style/style.module';
import { AppComponent } from './app.component';
import {
	PostComponent, EditPostBottomSheetComponent
} from './components/post/post.component';
import { PostListComponent } from './components/post-list/post-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { GuestComponent } from './components/guest/guest.component';
import {
	NavBarComponent, PostBottomSheetComponent
} from './components/nav-bar/nav-bar.component';


@NgModule({
	declarations: [
		AppComponent,
		PostComponent,
		PostListComponent,
		LoginComponent,
		RegisterComponent,
		HomeComponent,
		GuestComponent,
		NavBarComponent,
		PostBottomSheetComponent,
		EditPostBottomSheetComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule,
		FlexLayoutModule,
		BrowserAnimationsModule,
		StyleModule,
	],
	entryComponents: [
		PostBottomSheetComponent,
		EditPostBottomSheetComponent
	],
	providers: [
		...httpInterceptorProviders
	],
	bootstrap: [AppComponent]
})
export class AppModule {}
