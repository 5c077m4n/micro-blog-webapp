import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';

import { AuthService } from '../../services/auth.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { SharedService } from '../../services/shared/shared.service';
import { BlogService } from '../../services/blog.service';
import { SharedConstants } from '../../services/shared/shared.enum';
import { SnackBarService } from '../../services/snack-bar.service';


@Component({
	selector: 'app-nav-bar',
	templateUrl: './nav-bar.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
	constructor(
		private snack: SnackBarService,
		private bottomSheet: MatBottomSheet,
		private auth: AuthService,
		private router: Router,
		private messenger: SharedService,
	) {}
	ngOnInit() {}

	public home(): void {
		this.messenger.emit({
			type: SharedConstants.REFRESH_USER_POST_LIST
		});
	}
	public getTopPosts(): void {
		this.messenger.emit({
			type: SharedConstants.REFRESH_USER_TOP_POST_LIST
		});
	}
	public logout(): void {
		this.auth.logout();
		this.snack.show('See you next time!');
		this.router.navigate(['/guest']);
	}
	public PostBottomSheet(): void {
		this.bottomSheet.open(PostBottomSheetComponent);
	}
}


@Component({
	templateUrl: 'post-bottom-sheet.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PostBottomSheetComponent {
	public postControl: FormGroup;
	constructor(
		private bottomSheetRef: MatBottomSheetRef<PostBottomSheetComponent>,
		private blog: BlogService,
		private fb: FormBuilder,
		private auth: AuthService,
		private messenger: SharedService,
	) {
		this.initPostControl();
	}

	private initPostControl(): void {
		const userID = this.auth.getCurrentUser()._id;
		this.postControl = this.fb.group({
			userID: [userID, Validators.required],
			content: ['', Validators.required],
			usersVoteUp: [[userID]],
			usersVoteDown: [[]],
		});
	}
	onSubmit(e: MouseEvent): void {
		event.preventDefault();
		if(!this.postControl.valid) return;
		this.blog.addUserPost(this.postControl.value)
			.subscribe(_ => {
				this.bottomSheetRef.dismiss();
				this.messenger.emit({
					type: SharedConstants.REFRESH_USER_POST_LIST
				});
			});
	}
}
