import { Component, OnInit, Input, ChangeDetectionStrategy, Inject } from '@angular/core';
import {
	MatBottomSheet, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA
} from '@angular/material/bottom-sheet';

import { Post } from '../../models/post';
import { AuthService } from '../../services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BlogService } from '../../services/blog.service';
import { SharedService } from '../../services/shared/shared.service';
import { SharedConstants } from '../../services/shared/shared.enum';


@Component({
	selector: 'app-post',
	templateUrl: './post.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
	@Input() public post: Post;
	constructor(
		private blog: BlogService,
		private auth: AuthService,
		private messenger: SharedService,
		private bottomSheet: MatBottomSheet,
	) {}
	ngOnInit() {}

	public isLoggedIn(): boolean {
		return this.auth.isLoggedIn();
	}
	public isUsersPost(): boolean {
		return (this.post.userID === this.auth.getCurrentUser()._id);
	}
	public isUserInVoteUp(): boolean {
		return this.post.usersVoteUp.includes(this.auth.getCurrentUser()._id);
	}
	public isUserInVoteDown(): boolean {
		return this.post.usersVoteDown.includes(this.auth.getCurrentUser()._id);
	}
	public vote(direction: number): void {
		if(direction === +1) {
			if(this.isUserInVoteUp()) {
				this.blog.removeVoteUp(this.post._id)
					.subscribe(_ => this.messenger.emit({
						type: SharedConstants.REFRESH_USER_POST_LIST
					}));
			}
			else {
				this.blog.voteUp(this.post._id)
					.subscribe(_ => this.messenger.emit({
						type: SharedConstants.REFRESH_USER_POST_LIST
					}));
			}
		}
		if(direction === -1) {
			if(this.isUserInVoteDown()) {
				this.blog.removeVoteDown(this.post._id)
					.subscribe(_ => this.messenger.emit({
						type: SharedConstants.REFRESH_USER_POST_LIST
					}));
			}
			else {
				this.blog.voteDown(this.post._id)
					.subscribe(_ => this.messenger.emit({
						type: SharedConstants.REFRESH_USER_POST_LIST
					}));
			}
		}
	}
	public deletePost(): void {
		if(!this.isLoggedIn() || !this.isUsersPost()) return;
		else this.blog.deleteUserPost(this.post._id)
			.subscribe(_ => this.messenger.emit({
				type: SharedConstants.REFRESH_USER_POST_LIST
			}));
	}
	public EditPostBottomSheet(): void {
		if(!this.isLoggedIn() || !this.isUsersPost()) return;
		else this.bottomSheet.open(EditPostBottomSheetComponent, {
			data: this.post
		});
	}
}


@Component({
	templateUrl: 'edit-post-bottom-sheet.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EditPostBottomSheetComponent {
	public postControl: FormGroup;
	constructor(
		private bottomSheetRef: MatBottomSheetRef<EditPostBottomSheetComponent>,
		private blog: BlogService,
		private fb: FormBuilder,
		private messenger: SharedService,
		@Inject(MAT_BOTTOM_SHEET_DATA) public data: Post
	) {
		this.initPostControl(data);
	}

	private initPostControl(post: Post) {
		this.postControl = this.fb.group({
			_id: [post._id, Validators.required],
			userID: [post.userID, Validators.required],
			content: [post.content, Validators.required],
			usersVoteUp: [post.usersVoteUp],
			usersVoteDown: [post.usersVoteUp],
		});
	}
	onSubmit(e: MouseEvent): void {
		event.preventDefault();
		if(!this.postControl.valid) return;
		this.blog.editUserPost(this.postControl.value)
			.subscribe(_ => {
				this.bottomSheetRef.dismiss();
				this.messenger.emit({
					type: SharedConstants.REFRESH_USER_POST_LIST
				});
			});
	}
}
