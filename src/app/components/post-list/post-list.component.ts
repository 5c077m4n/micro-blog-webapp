import {
	Component,
	OnInit,
	OnDestroy,
	Input,
	ChangeDetectionStrategy,
	ChangeDetectorRef
} from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { Post } from '../../models/post';
import { User } from '../../models/user';
import { SharedConstants } from '../../services/shared/shared.enum';
import { BlogService } from '../../services/blog.service';
import { SharedService } from '../../services/shared/shared.service';


@Component({
	selector: 'app-post-list',
	templateUrl: './post-list.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
	styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy {
	@Input() public user: User;
	public posts: Post[];
	private listener: Subscription;
	constructor(
		private blog: BlogService,
		private messenger: SharedService,
		private cdr: ChangeDetectorRef,
	) {}
	ngOnInit() {
		if(this.user) this.userPost$.subscribe();
		else this.post$.subscribe();

		this.listener = this.messenger
			.subscribe(({ type, payload }) => {
				if(type === SharedConstants.REFRESH_POST_LIST)
					this.post$.subscribe();
				if(type === SharedConstants.REFRESH_USER_POST_LIST)
					this.userPost$.subscribe();
				if(type === SharedConstants.REFRESH_USER_TOP_POST_LIST)
					this.topPost$.subscribe();
			});
	}

	public get post$(): Observable<any> {
		return this.blog.getPosts().pipe(
			map(res => res && res.data),
			tap(posts => this.posts = posts),
			tap(() => this.cdr.detectChanges()),
		)
	}
	public get userPost$(): Observable<any> {
		return this.blog.getUserPosts().pipe(
			map(res => res && res.data),
			tap(posts => this.posts = posts),
			tap(() => this.cdr.detectChanges()),
		)
	}
	public get topPost$(): Observable<any> {
		return this.blog.getUserTopPosts().pipe(
			map(res => res && res.data),
			tap(posts => this.posts = posts),
			tap(() => this.cdr.detectChanges()),
		)
	}

	ngOnDestroy(): void {
		this.listener.unsubscribe();
	}
}
