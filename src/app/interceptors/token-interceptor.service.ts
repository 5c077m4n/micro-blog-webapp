import { Injectable } from '@angular/core';
import {
	HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpHeaders
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { tap, catchError, finalize } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

import { AuthService } from '../services/auth.service';
import { LocalStorageService } from '../services/local-storage.service';
import { LogService } from '../services/log.service';


@Injectable() export class TokenInterceptorService implements HttpInterceptor {
	private jwt: JwtHelperService;
	constructor(
		private auth: AuthService,
		private storage: LocalStorageService,
		private log: LogService,
	) {
		this.jwt = new JwtHelperService();
	}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		const startingTimestamp = Date.now();
		const token = this.auth.isLoggedIn()? this.auth.getToken() : '';
		const authReq = req.clone({
			headers: new HttpHeaders({
				'Access-Control-Allow-Origin': '*',
				'x-access-token': token,
				'Accept': 'application/json',
			}),
			body: req.body
		});
		return next.handle(authReq)
			.pipe(
				tap(async (event: HttpEvent<any>): Promise<void> => {
					if(event instanceof HttpResponse) {
						if(event.body && event.body.message === 'jwt expired')
							this.auth.logout();
						if(event.body && event.body.data && event.body.data.token) {
							const token = event.body.data.token;
							this.storage.add('raw-token', token);
							const userData = await Promise.resolve(
								this.jwt.decodeToken(token)
							);
							this.storage.add('current-user', userData);
						}
					}
				}),
				catchError(error => {
					if(error.status === 401) this.auth.logout();
					return throwError(error);
				}),
				finalize(() => {
					this.log.add(
						`${req.body}, ${req.url}, ${req.method}, duration: ${Date.now() - startingTimestamp}ms @ ${new Date().toLocaleString()}`
					);
				})
			);
	}
}
